<?php
/**
 * The template used for displaying contant page content
 *
 * @package urumall
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	/**
	 * Functions hooked in to urumall_page add_action
	 *
	 * @hooked urumall_page_header          	- 10
	 * @hooked urumall_empty_page_content       - 20
	 */
	do_action( 'urumall_empty_page' );
	?>
</div><!-- #post-## -->
